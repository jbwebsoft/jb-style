# jb_style

An AngularDart 1 Component for being re-used in other AngularDart projects

## Usage
1. Add the component to your pubspec.yaml
    - Use hosted Version

            dependencies:
              transmogrify:
                hosted:
                  name: transmogrify
                  url: http://some-package-server.com
                version: ^=1.0.0

    - Use git version

            dependencies:
              kittens:
                git: git://github.com/munificent/kittens.git

    - Git with specific branch

            dependencies:
              kittens:
                git:
                  url: git://github.com/munificent/kittens.git
                  ref: some-branch

2. Show your component to angular dependency injection -> see App Class in app_initialize.dart

        import 'package:jb_style/jb_style.dart';

        class App extends Angular.Module {

          App() {
            bind(JbStyle);
          }
        }

3. Add style to main css to hide jb-style content

        jb-style {
          display: none;
        }
