# Changelog

## 1.0.1
- found source for content showing up during load
  -> added hint to readme how to avoid this

## 1.0.0
- basic functionality release

## 0.0.1

- Initial version
