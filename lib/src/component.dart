library jb_style.component;

import 'package:angular/angular.dart';
import 'dart:html';
import 'dart:async';

@Component(
    selector: "jb-style",
    templateUrl: "packages/jb_style/src/component_template.html",
    useShadowDom: true
)
class JbStyle implements ShadowRootAware, ScopeAware {

  ContentElement _contentElement;

  @NgTwoWay("content")
  String content;

  Completer<bool> _scopeCompleter = new Completer();
  Completer<bool> _shadowRootCompleter = new Completer();

  Future<bool> _scopeAvailable;
  Future<bool> _shadowRootAvailable;

  Text textNode = new Text("");

  Scope _scope;
  ShadowRoot _shadowRoot;

  NgModel _ngModel;

  JbStyle(Element node, this._ngModel) {
    _scopeAvailable = _scopeCompleter.future;
    _shadowRootAvailable = _shadowRootCompleter.future;
  }


  @override
  void onShadowRoot(ShadowRoot shadowRoot) {
    _shadowRoot = shadowRoot;
    _shadowRootCompleter.complete(true);

    _scopeAvailable.then((_) {
      _contentElement = _shadowRoot.querySelector("content");
      textNode = _contentElement
          .getDistributedNodes()
          .first as Text;
      content = textNode.text;
    });
  }

  @override
  void set scope(Scope scope) {
    _scope = scope;
    _scopeCompleter.complete(true);

//    _scope.watch(":textNode.text", (newVal, oldVal) {
//      content = newVal;
//    });
  }

}